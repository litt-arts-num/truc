# coding: utf8

import os
import re

import modules.infos_json as infos_json

"""
Récupération de l'image docker et des technologies des projets utilisant docker.
:param projectsInfos:
:param projectsDocker:
"""
def getInfos(projectsInfos, projectsDocker):
    # parcours des projets qui utilisent docker
    for project in projectsDocker:
        projectPath = projectsInfos[project]["projectPath"]
        composerFound = False
        if projectPath != "":
            # recherche du docker-compose.yml
            if os.path.exists(projectPath + "/docker-compose.yml"):
                dockerCompose = open(projectPath + "/docker-compose.yml", "r")
                file = dockerCompose.read()
                # recherche de l'emplacement du Dockerfile
                searchWay = re.search("build:.*?([^\s]+)\s", file, re.S)
                if searchWay:
                    # recherche de l'image docker
                    if os.path.exists(projectPath + "/" + searchWay.group(1) + "/Dockerfile"):
                        dockerFile = open(projectPath + "/" + searchWay.group(1) + "/Dockerfile", "r")
                        searchDockerImage = re.match("FROM\s+([^\s]+)", dockerFile.readline())
                        if searchDockerImage:
                            projectsInfos[project]["dockerImage"] = searchDockerImage.group(1)
                        dockerFile.close()
                # recherche du composer.json
                searchWay = re.search("volumes:.*?([^\s]+):\/var\/www", file, re.S)
                if searchWay:
                    # recherche des technos
                    if os.path.exists(searchWay.group(1) + "/composer.json"):
                        projectsInfos[project]["technos"] = infos_json.getInfosComposer(searchWay.group(1) + "/composer.json")
                        composerFound = True
                dockerCompose.close()
            # recherche des technos
            if os.path.exists(projectPath + "/application"):
                # recherche du composer.json ailleurs si pas trouvé précédemment
                if not composerFound and os.path.exists(projectPath + "/application/composer.json"):
                    projectsInfos[project]["technos"] = infos_json.getInfosComposer(projectPath + "/application/composer.json")
                # recherche du package.json
                if os.path.exists(projectPath + "/application/package.json"):
                    projectsInfos[project]["technos"] = infos_json.getInfosPackage(projectPath + "/application/package.json")
