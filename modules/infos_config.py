# coding: utf8

import configparser
import os

"""
Récupère le lien du dépôt Git à l'aide du fichier config.
:param projectsInfos:
"""
def getInfos(projectsInfos):
    for project in projectsInfos:
        path = projectsInfos[project]["projectPath"] + "/.git/config"
        if path != "" and os.path.exists(path):
            config = configparser.ConfigParser()
            config.read(path)
            gitInfos = config['remote "origin"']['url']
            if gitInfos != "":
                if "@" in gitInfos:
                    git = gitInfos.split("@")[1]
                    urlGit = "https://" + git.replace(":", "/")
                else:
                    urlGit = gitInfos
                projectsInfos[project]["gitRepo"] = urlGit
