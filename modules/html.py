# coding: utf8

"""
Ecriture du header de l'html qui contiendra les résultats.
:return:
"""
def header(titlesList):
    rowTitle = ""
    for title in titlesList:
        rowTitle += "<th>" + title + "</th>"
    return  """
    <!doctype html>
    <html>
    <head>
    <title>TRUC</title>
    <meta charset="UTF-8">
    <link href=\"./node_modules/bootstrap/dist/css/bootstrap.min.css\" rel=\"stylesheet\"/>
    <link href=\"./assets/css/styles.css\" rel=\"stylesheet\"/>
    <script src=\"./node_modules/bootstrap/dist/js/bootstrap.min.js\"></script>
    <script src=\"./node_modules/jquery/dist/jquery.min.js\"></script>
    <script src=\"./node_modules/datatables/media/js/jquery.dataTables.min.js\"></script>
    <script src=\"./assets/js/function.js\"></script>
    </head>
    <body>
    <h1 class=\"text-center\">Tableau Récapitulatif des Usages et Connexions</h1>
    <br>
    <table id="table" class=\"table table-striped dt-responsive align-middle\">
    <thead class=\"table-dark\">
    <tr>
    """ + rowTitle + """
    </tr>
    </thead>
    <tbody>
    """

"""
Ecriture du footer de l'html qui contiendra les résultats.
:return:
"""
def footer():
    return """
    </tbody>
    </table>
    </body>
    </html>
    """
