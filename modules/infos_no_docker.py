# coding: utf8

import os

import modules.infos_json as infos_json

"""
Récupération des technologies des projets n'utilisant pas docker.
:param projectsInfos:
:param projectsNoDocker:
"""
def getInfos(projectsInfos, projectsNoDocker):
    # parcours des projets qui n'utilisent pas docker
    for project in projectsNoDocker:
        projectPath = projectsInfos[project]["projectPath"]
        # recherche des technos
        if projectPath != "":
            # recherche du composer.json
            if os.path.exists(projectPath + "/composer.json"):
                projectsInfos[project]["technos"] = infos_json.getInfosComposer(projectPath + "/composer.json")
            # recherche du package.json
            if os.path.exists(projectPath + "/package.json"):
                projectsInfos[project]["technos"] = infos_json.getInfosPackage(projectPath + "/package.json")
