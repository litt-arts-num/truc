# coding: utf8

import json

"""
Récupération des technologies et de leur version déclarées dans le composer.json
à partir d'une liste de technologies recherchées.
:param wayToComposer:
:return:
"""
def getInfosComposer(wayToComposer):
    # technos recherchées
    technosToSearch = ["bootstrap", "font-awesome", "php", "twig"]
    technosFound = []
    # print(wayToComposer)
    composer = open(wayToComposer, "r")
    composerData = json.load(composer)
    if "require" in composerData.keys():
        for techno in composerData["require"].keys():
            for tech in techno.split("/"):
                if tech in technosToSearch:
                    # [1:] ->  ne pas prendre le '^' devant le numéro de version
                    technosFound.append(tech + " v" + composerData["require"][techno][1:])
                    # évite les doublons ex : "twig/twig"
                    break
    composer.close()
    return technosFound

"""
Récupération des technologies et de leur version déclarées dans le package.json
sauf celles déclarées comme non recherchées.
:param wayToComposer:
:return:
"""
def getInfosPackage(wayToComposer):
    # technos non recherchées
    technosToNotSearch = ["popper.js"]
    technosFound = []
    # print(wayToComposer)
    composer = open(wayToComposer, "r")
    composerData = json.load(composer)
    if "dependencies" in composerData.keys():
        for techno in composerData["dependencies"].keys():
            if "/" in techno:
                tech = techno.split("/")[-1]
            else :
                tech = techno
            if techno not in technosToNotSearch:
                # [1:] ->  ne pas prendre le "^" devant le numéro de version
                technosFound.append(tech + " v" + composerData["dependencies"][techno][1:])
    composer.close()
    return technosFound
