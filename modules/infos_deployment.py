# coding: utf8

import re

"""
Récupération du nom de serveur, du numéro de port et de proxyPass, et du chemin
du dossier des projets à partir des fichiers de configuration
Liste les projets utilisant et n'utilisant pas docker séparément.
:param filesConfWay:
:param filesConf:
:return:
"""
def getInfos(filesConfWay, filesConf):

    # listes des serverName (car clef de projectsInfos) sur lesquels docker est utilisé ou non
    projectsDocker = []
    projectsNoDocker = []
    # {serverName => {infoType => info}}
    projectsInfos = {}

    for file in filesConf:
        fileContent = open(filesConfWay + file, "r")
        # variables recherchées
        serverName = ""
        port = ""
        proxyPass = ""
        projectPath = ""
        # recherche des infos de nom de domaine, port, proxyPass et nom de répertoire
        for line in fileContent:
            if serverName == "":
                searchServerName = re.search("ServerName\s([^\s]+)", line)
                if searchServerName:
                    serverName = searchServerName.group(1)
            if port == "":
                searchPort = re.search("<VirtualHost\s\*:(\d+)>", line)
                if searchPort:
                    port = searchPort.group(1)
            if proxyPass == "":
                searchProxyPass = re.search("ProxyPass\s[^\s]+\s[^\s]+:(\d+)", line)
                if searchProxyPass:
                    proxyPass = searchProxyPass.group(1)
            if projectPath == "":
                searchDocumentRoot = re.search("DocumentRoot\s([^\s]+)", line)
                if searchDocumentRoot:
                    projectPath = searchDocumentRoot.group(1)
                    if projectPath != "":
                        # cas où le chemin se termine par un slash -> l'enlever pour récupérer le nom de dossier
                        if projectPath[-1] == "/":
                            projectPath = projectPath[0:-1]
        if proxyPass != "":
            projectsDocker.append(serverName)
        else:
            projectsNoDocker.append(serverName)

        # remplissage de projectsInfos
        projectsInfos[serverName] = {
            "port" : port,
            "projectPath" : projectPath,
            "proxyPass" : proxyPass,
            "serverName" : serverName
        }

        fileContent.close()

    return projectsInfos, projectsDocker, projectsNoDocker
