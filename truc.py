# coding: utf8

import os
import re
import sys
import webbrowser

import modules.html as html
import modules.infos_deployment as infos_deployment
import modules.infos_docker as infos_docker
import modules.infos_no_docker as infos_no_docker
import modules.infos_config as infos_config

if __name__ == '__main__':

    projectsWay = sys.argv[1]
    # cas où le chemin ne se termine pas par un slash -> l'ajouter
    if projectsWay[-1] != "/":
        projectsWay += "/"
    projectsFolders = os.listdir(projectsWay)

    filesConfWay = "/etc/apache2/sites-enabled/"
    # test en local
    # filesConfWay = "/etc/apache2/sites-available/"
    filesConf = os.listdir(filesConfWay);

    # projectsInfos : ensemble des informations connues sur les projets de type
    # {serverName => {infoType => info}}
    # projectsDocker : liste des serverName des projets utilisant docker
    # projectsDocker : liste des serverName des projets n'utilisant pas docker
    # -> distinction car traitements différents
    projectsInfos, projectsDocker, projectsNoDocker = infos_deployment.getInfos(filesConfWay, filesConf)

    # récup les noms de répertoire pour les projets en parcourant les dossiers
    # et comparant le proxyPass du .env au proxyPass du fichier conf
    for projectRep in projectsFolders:
        if os.path.exists(projectsWay + projectRep + "/.env"):
            env = open(projectsWay + projectRep + "/.env")
            envContent = env.read()
            searchPort = re.search("(\w+)=(\d+).(\w+)=(\d+)?", envContent, re.S)
            if searchPort:
                if searchPort.group(1)=="PORTAPACHE":
                    for project in projectsInfos:
                        if projectsInfos[project]["proxyPass"] == searchPort.group(2):
                            projectsInfos[project]["projectPath"] = projectsWay + projectRep
                            if searchPort.group(3)=="PORTADMINER" and searchPort.group(4):
                                projectsInfos[project]["dataBasePort"] = searchPort.group(4)

            env.close()

    # Récupération du nom de projet via le nom de domaine
    for project in projectsInfos:
        projectsInfos[project]["projectRepName"] = projectsInfos[project]["serverName"].split(".")[0]

    # Met à jour les informations à propos des projet utilisant docker en
    # recherchant l'image docker et les technos utilisées
    infos_docker.getInfos(projectsInfos, projectsDocker)
    # Met à jour les informations à propos des projet n'utilisant pas docker en
    # recherchant les technos utilisées
    infos_no_docker.getInfos(projectsInfos, projectsNoDocker)

    infos_config.getInfos(projectsInfos)

    # Liste des titres des colonnes
    titlesList = ["Nom du projet", "Port apache", "Port bdd", "Port d'écoute", "Nom de domaine", "Technologies utilisées", "Image docker", "Emplacement du projet", "Dépôt Git"]
    # Création du fichier des résultats
    fileOut = open('output/index.html', 'w', encoding='utf-8')
    htmlBegin = html.header(titlesList)
    htmlContent = ""
    htmlEnd = html.footer()

    # Complétion du fichier des résultats
    # print(projectsInfos)
    for key in sorted(projectsInfos.keys()):
        print(key)
        htmlContent += "<tr style=\"vertical-align:middle\">"
        for infoType in ["projectRepName","proxyPass","dataBasePort","port","serverName","technos","dockerImage","projectPath", "gitRepo"]:
            if infoType in projectsInfos[key].keys():
                # Cas des technos : une liste de données
                if type(projectsInfos[key][infoType]) == list:
                    htmlContent += "<td><ul class=\"list-group list-group-flush\">"
                    for techno in projectsInfos[key][infoType]:
                        htmlContent += "<li class=\"list-group-item\">" + techno + "</li>"
                    htmlContent += "</ul></td>"
                # Cas d'une donnée classique (chaîne non vide)
                elif projectsInfos[key][infoType] != "":
                    # Cas du nom de domaine : le mettre sous forme de lien
                    if infoType == "serverName":
                        htmlContent += "<td><a href=\"http://" + projectsInfos[key][infoType] + "\" target=\"_blank\">" + projectsInfos[key][infoType] + "</a></td>"
                    elif infoType == "gitRepo":
                        htmlContent += "<td><a href=" + projectsInfos[key][infoType] + " target=\"_blank\"> <img src=\"./assets/img/gitlab.png\" width=\"50rem\" title=\"" + projectsInfos[key][infoType] + "\"/></a></td>"
                    else:
                        htmlContent += "<td>" + projectsInfos[key][infoType] + "</td>"
                # Cas d'une donnée vide
                else:
                    htmlContent += "<td> - </td>"
            # Cas d'une donnée non renseignée
            else:
                htmlContent += "<td> - </td>"
        htmlContent += "</tr>"

    # Ecriture et fermeture du fichier des résultats
    fileOut.write(htmlBegin + htmlContent + htmlEnd)
    fileOut.close()
