# Installation des dépendances de truc

`npm install`

> Attention à chaque modification du code, bien penser à refaire l'installation des dépendances avant de relancer truc.

# Lancement de truc

`python3 truc.py nomRep`

Où `nomRep` est le chemin vers l'ensemble des répertoires des projets.
